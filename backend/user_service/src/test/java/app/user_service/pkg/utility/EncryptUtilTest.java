package app.user_service.pkg.utility;


import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
@SpringBootTest
public class EncryptUtilTest {
    @Test
    public void encryptDecryptAesCbc(){
        String key = "*e-EncryptionKey";
        String iv = "1-frgjkhltiofpc*";
        String txt = "hello world 123 () [] \\/ | -!@#$%^&*()_+";

        try {
            // String encrypt = encryptAesCbc(txt, key, iv);

            String decrypt = decryptAesCbc("yGt3MtDDxgZzHeMIodw+ug==", key, iv);

            // System.out.println("encrypt : "+encrypt);
            System.out.println("decrypt : "+decrypt);
            System.out.println("is decrypt success : "+(decrypt.equals(txt)));
        } catch (Exception e) {
            // TODO Auto-generated catch blockd
            e.printStackTrace();
            System.out.println(e.getMessage());
        }


    }

    public String encryptAesCbc(String plainTxt, String key, String iv) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
        IvParameterSpec ivSpec = new IvParameterSpec(iv.getBytes("UTF-8"));
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING"); 
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] encrypted = cipher.doFinal(plainTxt.getBytes());
        return Base64.encodeBase64String(encrypted);
    }

    public String decryptAesCbc(String encryptedTxt, String key, String iv) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
        IvParameterSpec ivSpec = new IvParameterSpec(iv.getBytes("UTF-8"));
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING"); 
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

        byte[] plainTxt = cipher.doFinal(Base64.decodeBase64(encryptedTxt));
        
        return new String(plainTxt);
    }

}
