package app.user_service.pkg.utility;

import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.*;
import javax.crypto.spec.*;

import org.apache.tomcat.util.codec.binary.Base64;

public class CryptoUtil {
    public static String encryptAesCbc(String plainTxt, String key, String iv) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
        Cipher cipher = getCipherAesCbc(key, iv, Cipher.ENCRYPT_MODE);

        byte[] encrypted = cipher.doFinal(plainTxt.getBytes());
        return Base64.encodeBase64String(encrypted);
    }

    public static String decryptAesCbc(String encryptedTxt, String key, String iv) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
        Cipher cipher = getCipherAesCbc(key, iv, Cipher.DECRYPT_MODE);

        byte[] plainTxt = cipher.doFinal(Base64.decodeBase64(encryptedTxt));
        return new String(plainTxt);
    }

    public static Cipher getCipherAesCbc(String key, String iv, int mode) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException{
        IvParameterSpec ivSpec = new IvParameterSpec(iv.getBytes("UTF-8"));
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING"); 
        cipher.init(mode, keySpec, ivSpec);

        return cipher;
    }

}
