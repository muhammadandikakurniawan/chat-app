package app.user_service.repository.postgresql;

import java.sql.Timestamp;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import app.user_service.domain.entity.UserEntity;
import app.user_service.domain.repository.IUserRepository;

@Component
public class UserRepository extends BaseRepository<UserEntity> implements IUserRepository {

    @PersistenceUnit()
	private EntityManagerFactory _emf;

    @PersistenceContext
    private EntityManager _em;

    @Override
    public UserEntity create(UserEntity data) {
        if(Optional.of(data).isEmpty()){
            return null;
        }
        
        Timestamp now = new Timestamp((new Date().getTime()));
        data.setId(UUID.randomUUID().toString());
        data.setCreatedAt(now);

        data = super.create(data);

        return data;
    }

    @Override
    public UserEntity update(UserEntity data){
        if(Optional.of(data).isEmpty()){
            return null;
        }
        
        Timestamp now = new Timestamp((new Date().getTime()));
        data.setUpdatedAt(now);

        return super.update(data);
    }

    @Override
    public UserEntity findByEmail(String email) {

        UserEntity data = null;

        try{
            data = _em.createQuery("SELECT user FROM UserEntity user WHERE user.email = :email AND deleted_at IS NULL", UserEntity.class)
            .setParameter("email", email)
            .getSingleResult();
        }catch(NoResultException ex){
            return null;
        }

        return data;
    }
    
}
