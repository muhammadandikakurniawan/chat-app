package app.user_service.repository.postgresql;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import org.springframework.stereotype.Component;

import java.util.*;

import app.user_service.domain.repository.IBaseRepository;

@Component("BasePostgreSQlRepository")
public class BaseRepository<T> implements IBaseRepository<T> {
    @PersistenceUnit()
	private EntityManagerFactory _emf;

    @PersistenceContext
    private EntityManager _em;

    @Override
    public T create(T data) {
        if(Optional.of(data).isEmpty()){
            return null;
        }
        
        EntityManager em = _emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        tx.begin();
        em.persist(data);
        tx.commit();

        em.close();
        return data;
    }

    @Override
    public T update(T data){
        EntityManager em = _emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        data = em.merge(data);
        tx.commit();

        
        return data;
    }
}
