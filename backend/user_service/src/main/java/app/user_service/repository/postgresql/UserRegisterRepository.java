package app.user_service.repository.postgresql;

import java.sql.Timestamp;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import app.user_service.domain.entity.UserEntity;
import app.user_service.domain.entity.UserRegisterEntity;
import app.user_service.domain.repository.IUserRegisterRepository;
import app.user_service.domain.repository.IUserRepository;

@Component
public class UserRegisterRepository extends BaseRepository<UserRegisterEntity> implements IUserRegisterRepository {

    @PersistenceUnit()
	private EntityManagerFactory _emf;

    @PersistenceContext
    private EntityManager _em;

    @Override
    public UserRegisterEntity create(UserRegisterEntity data) {
        if(Optional.of(data).isEmpty()){
            return null;
        }
        
        Timestamp now = new Timestamp((new Date().getTime()));
        data.setId(UUID.randomUUID().toString());
        data.setCreatedAt(now);

        return super.create(data);
    }

    @Override
    public UserRegisterEntity update(UserRegisterEntity data){
        if(Optional.of(data).isEmpty()){
            return null;
        }
        
        Timestamp now = new Timestamp((new Date().getTime()));
        data.setUpdatedAt(now);

        return super.update(data);
    }

    @Override
    public UserRegisterEntity findByEmail(String email) {

        UserRegisterEntity data = null;

        try{
            data = _em.createQuery("SELECT user FROM UserRegisterEntity user WHERE user.email = :email AND deleted_at IS NULL", UserRegisterEntity.class)
            .setParameter("email", email)
            .getSingleResult();
        }catch(NoResultException ex){
            return null;
        }

        return data;
    }

    @Override
    public UserRegisterEntity findByEmailCode(String email, String code) {

        UserRegisterEntity data = null;

        try{
            data = _em.createQuery("SELECT user FROM UserRegisterEntity user WHERE user.email = :email AND user.activationCode = :code AND deleted_at IS NULL", UserRegisterEntity.class)
            .setParameter("email", email)
            .setParameter("code", code)
            .getSingleResult();
        }catch(NoResultException ex){
            return null;
        }

        return data;
    }
    
}
