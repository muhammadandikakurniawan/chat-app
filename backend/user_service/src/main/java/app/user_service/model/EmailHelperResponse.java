package app.user_service.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Getter
@Setter
public class EmailHelperResponse {
    
    @JsonProperty("status_code")
    private boolean success = true;

    @JsonProperty("message")
    private String message;

}
