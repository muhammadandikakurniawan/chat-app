package app.user_service.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BaseResponseModel<T> {
    @JsonProperty("status_code")
    private String statusCode = "200";

    @JsonProperty("message")
    private String message;

    @JsonProperty("error_message")
    private String errorMessage;

    @JsonProperty("success")
    private boolean success = true;

    @JsonProperty("data")
    private T data;

    public void setError(String errMsg){
        this.success = false;
        this.errorMessage = errMsg;
    }

    public void setProp(String statusCode, String message, String errMsg, boolean success, T data){
        this.statusCode = statusCode;
        this.message = message;
        this.errorMessage = errMsg;
        this.success = success;
        this.data = data;
    }

    public void setProp(String statusCode, String message, String errMsg, boolean success){
        this.statusCode = statusCode;
        this.message = message;
        this.errorMessage = errMsg;
        this.success = success;
    }

    public void setError(Exception ex, String errMsg){
        this.statusCode = "500";
        this.setError(errMsg);
        this.errorMessage = ex.getMessage();
    }

}
