package app.user_service.configuration;

import java.util.HashMap;

import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    // basePackages = "skuyservice.repository.postgresql",
    // entityManagerFactoryRef = "skuyserviceEntityManager", 
    // transactionManagerRef = "skuyserviceTransactionManager"
)
public class PostgreSqlConfiguration {
    
    @Autowired
    private Environment env;

    @Bean
    public DataSource userServiceDs(){
        DriverManagerDataSource ds = new DriverManagerDataSource();

        String host = env.getProperty("db.postgresql.host");
        int port = env.getProperty("db.postgresql.port", int.class);
        String username = env.getProperty("db.postgresql.user");
        String password = env.getProperty("db.postgresql.password");
        String db = env.getProperty("db.postgresql.db");
        
        String url = String.format("jdbc:postgresql://%s:%d/%s", host, port, db);

        ds.setUrl(url);
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUsername(username);
        ds.setPassword(password);

        return ds;
    }

    @Bean
    public JdbcTemplate userServiceJdbcTemplate(DataSource ds){
        return new JdbcTemplate(ds);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean userServiceEntityManager(DataSource ds){
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(ds);
        em.setPackagesToScan("app.user_service.domain.entity");
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);

        HibernateJpaVendorAdapter vendorAdatper = new HibernateJpaVendorAdapter();
        vendorAdatper.setDatabase(Database.POSTGRESQL);

        em.setJpaVendorAdapter(vendorAdatper);
        em.setPersistenceUnitName("userService");

        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.format_sql", true);
        properties.put("spring.jpa.hibernate.ddl.auto","update");
        properties.put("spring.jpa.show-sql",true);
        properties.put("spring.jpa.properties.hibernate.dialect","org.hibernate.dialect.PostgreSQLDialect");
        properties.put("spring.jpa.hibernate.naming.implicit-strategy","org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyJpaImpl");
        properties.put("spring.jpa.hibernate.naming.physical-strategy","org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");

        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    public PlatformTransactionManager userServiceTransactionManager(LocalContainerEntityManagerFactoryBean em){
        JpaTransactionManager tm = new JpaTransactionManager();
        tm.setEntityManagerFactory(em.getObject());
        return tm;
    }
}