package app.user_service.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import app.user_service.helper.EmailHelper;

@Configuration
public class HelperConfiguration {
    @Autowired
    private Environment env;

    @Bean
    public EmailHelper getEmailHelper(){
        return new EmailHelper(env);
    }
}
