package app.user_service.domain.repository;

import app.user_service.domain.entity.UserRegisterEntity;

public interface IUserRegisterRepository extends IBaseRepository<UserRegisterEntity>{
    public UserRegisterEntity findByEmail(String email);
    public UserRegisterEntity findByEmailCode(String email, String code);
}
