package app.user_service.domain.repository;

import app.user_service.domain.entity.UserEntity;

public interface IUserRepository extends IBaseRepository<UserEntity>{
    public UserEntity findByEmail(String email);
}
