package app.user_service.domain.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.*;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity {
    @Column(name = "created_at")
    private Timestamp CreatedAt;

    @Column(name = "deleted_at")
    private Timestamp DeletedAt;

    @Column(name = "updated_at")
    private Timestamp UpdatedAt;
}
