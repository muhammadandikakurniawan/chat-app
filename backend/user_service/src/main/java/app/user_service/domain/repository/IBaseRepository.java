package app.user_service.domain.repository;

public interface IBaseRepository<T> {
    public T create(T data);
    public T update(T data);
}
