package app.user_service.usecase.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Setter
@Getter
public class ActivateAccountRequest {
    
    @JsonProperty("email")
    private String email;

    @JsonProperty("activation_code")
    private String activationCode;

    @JsonProperty("password")
    private String password;

}
