package app.user_service.usecase.user;

import app.user_service.model.BaseResponseModel;
import java.util.concurrent.CompletableFuture;

import app.user_service.usecase.user.model.*;

public interface IUserUsecase {
    public CompletableFuture<BaseResponseModel<UserModel>> login(LoginRequest request);
    public CompletableFuture<BaseResponseModel<UserModel>> activateAccount(ActivateAccountRequest request);
}
