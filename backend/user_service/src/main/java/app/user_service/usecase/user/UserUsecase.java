package app.user_service.usecase.user;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import app.user_service.domain.entity.UserEntity;
import app.user_service.domain.entity.UserRegisterEntity;
import app.user_service.helper.EmailHelper;
import app.user_service.model.BaseResponseModel;
import app.user_service.pkg.utility.CryptoUtil;
import app.user_service.repository.postgresql.*;

import app.user_service.usecase.user.model.*;

@Service
public class UserUsecase implements IUserUsecase{

    @Autowired
    private Environment env;

    @Autowired
    private UserRepository _userRepository;

    @Autowired
    private UserRegisterRepository _userRegisterRepository;

    @Autowired
    private EmailHelper _emailHelper;

    @Override
    @Async
    public CompletableFuture<BaseResponseModel<UserModel>> login(LoginRequest request) {
        BaseResponseModel<UserModel> response = new BaseResponseModel<UserModel>();
        
       try{
            String aesKey = env.getProperty("crypto.user_password_key");
            String aesIv = env.getProperty("crypto.user_password_iv");
            String plainRequestPassword = CryptoUtil.decryptAesCbc(request.getPassword(), aesKey, aesIv);

            UserEntity user = this._userRepository.findByEmail(request.getEmail());
           
            if(user == null){
                String activationCode = RandomStringUtils.randomAlphanumeric(7);

                UserRegisterEntity userRegisData = new UserRegisterEntity();
                userRegisData.setEmail(request.getEmail());
                userRegisData.setPassword(request.getPassword());
                userRegisData.setActivationCode(activationCode);

                this._userRegisterRepository.create(userRegisData);

                response.setStatusCode("10");
                response.setMessage("account is inactive");
                CompletableFuture.runAsync(() -> _emailHelper.SendBlastEmail("chat-app", "awan corp", "Activation code : "+activationCode, request.getEmail()));
                return CompletableFuture.completedFuture(response);
            }

            String plainPassword = CryptoUtil.decryptAesCbc(user.getPassword(), aesKey, aesIv);
            if(!plainPassword.equals(plainRequestPassword)){
                response.setProp("20", "password wrong", "", false);
                return CompletableFuture.completedFuture(response);
            }

            Timestamp now = new Timestamp((new Date().getTime()));
            user.setLastLoginAt(now);

            UserModel dataUserResp = new UserModel();
            dataUserResp.setEmail(user.getEmail());
            dataUserResp.setId(user.getId());
            dataUserResp.setUsername(user.getUsername());
            response.setData(dataUserResp);

            _userRepository.update(user);

       }catch(Exception ex){
            response.setError(ex, "error");
       }

        return CompletableFuture.completedFuture(response);
    }

    @Override
    public CompletableFuture<BaseResponseModel<UserModel>> activateAccount(ActivateAccountRequest request) {
        BaseResponseModel<UserModel> response = new BaseResponseModel<UserModel>();

        try{
            String aesKey = env.getProperty("crypto.user_password_key");
            String aesIv = env.getProperty("crypto.user_password_iv");
            String plainRequestPassword = CryptoUtil.decryptAesCbc(request.getPassword(), aesKey, aesIv);

            UserRegisterEntity userRegister = _userRegisterRepository.findByEmailCode(request.getEmail(), request.getActivationCode());
            if(userRegister == null){
                response.setProp("24", "user not found", "", false);
                return CompletableFuture.completedFuture(response);
            }

            String plainPassword = CryptoUtil.decryptAesCbc(userRegister.getPassword(), aesKey, aesIv);
            if(!plainPassword.equals(plainRequestPassword)){
                response.setProp("20", "password wrong", "", false);
                return CompletableFuture.completedFuture(response);
            }

            Timestamp now = new Timestamp((new Date().getTime()));
            userRegister.setActivateAt(now);



            _userRegisterRepository.update(userRegister);

            UserEntity user = new UserEntity();
            user.setEmail(userRegister.getEmail());
            user.setPassword(userRegister.getPassword());
            user = _userRepository.create(user);

            UserModel dataUserResp = new UserModel();
            dataUserResp.setEmail(user.getEmail());
            dataUserResp.setId(user.getId());
            dataUserResp.setUsername(user.getUsername());
            response.setData(dataUserResp);

        }catch(Exception ex){
            response.setError(ex, "error");
        }

        return CompletableFuture.completedFuture(response);
    }

    
}
