package app.user_service.helper;

import java.util.concurrent.CompletableFuture;
import java.util.*;
import java.util.concurrent.ExecutionException;

import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Async;

import app.user_service.model.EmailHelperResponse;

public class EmailHelper {

    private String _username;
    private String _password;
    private String _host;
    private int _port;

    public EmailHelper(Environment env){
        this._username = env.getProperty("email.username");
        this._password = env.getProperty("email.password");
        this._host = env.getProperty("email.host");
        this._port = env.getProperty("email.port", int.class);
    }

    @Async
    public CompletableFuture<EmailHelperResponse> SendBlastEmail(String from, String subject, String msg, String... to) {
        EmailHelperResponse result = new EmailHelperResponse();

        try {
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setHost(_host);
            mailSender.setPort(_port);

            mailSender.setUsername(_username);
            mailSender.setPassword(_password);

            Properties mailSenderProps = mailSender.getJavaMailProperties();
            mailSenderProps.put("mail.transport.protocol", "smtp");
            mailSenderProps.put("mail.smtp.auth", "true");
            mailSenderProps.put("mail.smtp.starttls.enable", "true");
            mailSenderProps.put("mail.debug", "true");

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom(from);
            mailMessage.setTo(to);
            mailMessage.setSubject(subject);
            mailMessage.setText(msg);

            mailSender.send(mailMessage);

        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
        }

        return CompletableFuture.completedFuture(result);
    }


    @Async
    public CompletableFuture<EmailHelperResponse> SendMassEmail(String from, String subject, List<String> to, String msg) {
        EmailHelperResponse result = new EmailHelperResponse();

        try {

            to.forEach(el -> {
                try {
                    this.SendBlastEmail(from, subject, msg, el).get();
                } catch (InterruptedException | ExecutionException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            });

        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
        }

        return CompletableFuture.completedFuture(result);
    }

}
