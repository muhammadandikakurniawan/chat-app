package app.user_service.delivery.grpc.handler;
import app.user_service.delivery.grpc.middleware.AppKeyMiddleware;
import app.user_service.grpc.user_service.UserServiceGrpc.UserServiceImplBase;
import app.user_service.model.BaseResponseModel;
import app.user_service.usecase.user.UserUsecase;
import app.user_service.usecase.user.model.ActivateAccountRequest;
import app.user_service.usecase.user.model.LoginRequest;
import app.user_service.usecase.user.model.UserModel;
import io.grpc.stub.StreamObserver;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;


@GRpcService(applyGlobalInterceptors = true, interceptors = {AppKeyMiddleware.class})
public class UserServiceHandler extends UserServiceImplBase{

    @Autowired
    private UserUsecase _userUsecase;

    @Override
    public void login(app.user_service.grpc.user_service.LoginRequest request, StreamObserver<app.user_service.grpc.user_service.LoginResponse> responseObserver){
        app.user_service.grpc.user_service.LoginResponse.Builder response = app.user_service.grpc.user_service.LoginResponse.newBuilder();

        LoginRequest in = new LoginRequest();
        in.setEmail(request.getEmail()); 
        in.setPassword(request.getPassword()); 

        try {
            BaseResponseModel<UserModel> out = _userUsecase.login(in).get();

            if(out.getData() != null){
                app.user_service.grpc.user_service.UserModel.Builder dataUser = app.user_service.grpc.user_service.UserModel.newBuilder();
                dataUser.setEmail(Optional.ofNullable(out.getData().getEmail()).orElse(""));
                dataUser.setId(Optional.ofNullable(out.getData().getUsername()).orElse(""));
                dataUser.setId(Optional.ofNullable(out.getData().getId()).orElse(""));

                response.setData(dataUser);
            }

            response.setStatusCode(Optional.ofNullable(out.getStatusCode()).orElse(""));
            response.setSuccess(Optional.ofNullable(out.isSuccess()).orElse(false));
            response.setMessage(Optional.ofNullable(out.getMessage()).orElse(""));
            response.setErrorMessage(Optional.ofNullable(out.getErrorMessage()).orElse(""));

        } catch (InterruptedException | ExecutionException e) {
            response.setStatusCode("500");
            response.setSuccess(false);
            response.setMessage("error");
            response.setErrorMessage(e.getMessage());
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void activateAccount(app.user_service.grpc.user_service.ActivateAccountRequest request, StreamObserver<app.user_service.grpc.user_service.ActivateAccountResponse> responseObserver) {
        app.user_service.grpc.user_service.ActivateAccountResponse.Builder response = app.user_service.grpc.user_service.ActivateAccountResponse.newBuilder();

        ActivateAccountRequest in = new ActivateAccountRequest();
        in.setEmail(request.getEmail()); 
        in.setPassword(request.getPassword()); 
        in.setActivationCode(request.getActivationCode()); 

        try {
            BaseResponseModel<UserModel> out = _userUsecase.activateAccount(in).get();

            if(out.getData() != null){
                app.user_service.grpc.user_service.UserModel.Builder dataUser = app.user_service.grpc.user_service.UserModel.newBuilder();
                dataUser.setEmail(Optional.ofNullable(out.getData().getEmail()).orElse(""));
                dataUser.setId(Optional.ofNullable(out.getData().getUsername()).orElse(""));
                dataUser.setId(Optional.ofNullable(out.getData().getId()).orElse(""));

                response.setData(dataUser);
            }

            response.setStatusCode(Optional.ofNullable(out.getStatusCode()).orElse(""));
            response.setSuccess(Optional.ofNullable(out.isSuccess()).orElse(false));
            response.setMessage(Optional.ofNullable(out.getMessage()).orElse(""));
            response.setErrorMessage(Optional.ofNullable(out.getErrorMessage()).orElse(""));

        } catch (InterruptedException | ExecutionException e) {
            response.setStatusCode("500");
            response.setSuccess(false);
            response.setMessage("error");
            response.setErrorMessage(e.getMessage());
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

}
