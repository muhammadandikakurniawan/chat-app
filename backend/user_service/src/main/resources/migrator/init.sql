CREATE DATABASE user_service;

CREATE TABLE IF NOT EXISTS users(
	id VARCHAR(255) PRIMARY KEY,
	username VARCHAR(255) DEFAULT NULL,
	password VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	last_login TIMESTAMP DEFAULT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT NULL,
	deleted_at TIMESTAMP DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS user_register(
	id VARCHAR(255) PRIMARY KEY,
	username VARCHAR(255) DEFAULT NULL,
	password VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	activation_code VARCHAR(255) NOT NULL,
	activate_at TIMESTAMP DEFAULT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT NULL,
	deleted_at TIMESTAMP DEFAULT NULL
);