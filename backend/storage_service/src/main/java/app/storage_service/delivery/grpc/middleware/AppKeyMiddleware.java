package app.storage_service.delivery.grpc.middleware;

import org.lognet.springboot.grpc.GRpcGlobalInterceptor;

import io.grpc.Metadata.*;
import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Status;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;

@GRpcGlobalInterceptor()
public class AppKeyMiddleware implements ServerInterceptor {

    public static final Context.Key<Object> API_KEY = Context.key("x-api-key");

    @Override
    public <ReqT, RespT> Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        String oldP = (String) API_KEY.get();
        Key<String> appKeyHeaderKey = Metadata.Key.of("x-api-key",Metadata.ASCII_STRING_MARSHALLER);
        String apiKey = headers.get(appKeyHeaderKey);
        // if(!apiKey.equals("chat-app-api-key")){
        //     call.close(Status.UNAVAILABLE, headers);

        //     return new ServerCall.Listener<ReqT>(){
        //     };
        // }

        Context ctx = Context.current().withValue(API_KEY, apiKey);
        return Contexts.interceptCall(ctx, call, headers, next);
    }
    
}
