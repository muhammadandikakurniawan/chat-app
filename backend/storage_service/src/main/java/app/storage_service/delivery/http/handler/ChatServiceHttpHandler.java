package app.storage_service.delivery.http.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import com.google.protobuf.ByteString;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import app.storage_service.grpc.chat_service.UploadFileRequest;
import app.storage_service.grpc.chat_service.UploadFileResponse;
import app.storage_service.grpc.chat_service.ChatServiceGrpc.ChatServiceStub;
import app.storage_service.grpc.chat_service.UploadFileRequest.FileInfo;
import app.storage_service.helper.MinioHelper;
import io.grpc.stub.StreamObserver;
import io.minio.GetObjectResponse;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "api/chat")
public class ChatServiceHttpHandler {

    private ChatServiceStub _chatServiceGrpcStub;
    private MinioHelper _minioHelper;

    public ChatServiceHttpHandler(
        ChatServiceStub chatServiceGrpcStub,
        MinioHelper minioHelper
    ){
        _chatServiceGrpcStub = chatServiceGrpcStub;
        _minioHelper = minioHelper;
    }

    
    @RequestMapping(value = "/read-file/{bucket}/{object}", method = RequestMethod.GET)
    public void readFile(HttpServletResponse response, @PathVariable("bucket") String bucket, @PathVariable("object") String object) throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException{
        // ResponseEntity response = new ResponseEntity<>("File not found", HttpStatus.OK);
        
        GetObjectResponse res = _minioHelper.getFile(bucket, object);
        InputStream fileStream = res;
        byte[] out = fileStream.readAllBytes(); 
        IOUtils.copy(res.nullInputStream(), response.getOutputStream());
        
        // if ( !res.object().equals("")){
            


        //     HttpHeaders headers = new HttpHeaders();
        //     // download = attachment, preview = inline
            
        //     headers.add("Content-Disposition", "inline; filename="+res.object());
        //     // headers.add("Content-Type", res.headers().get("content-type"));
        //     response = ResponseEntity.ok()
        //     .headers(headers)
        //     .contentLength(out.length)
        //     .body(out);
        // }
        
        // return  response;
    }

    @PostMapping("/upload-file")
    public Object uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("bucket") String bucket){

        StreamObserver<UploadFileResponse> response = new StreamObserver<UploadFileResponse>(){

            @Override
            public void onNext(UploadFileResponse value) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void onError(Throwable t) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void onCompleted() {
                // TODO Auto-generated method stub
                
            }
            
        };

        StreamObserver<UploadFileRequest> streamObserver = _chatServiceGrpcStub.uploadFile(response);

        String[] chunkFileName = file.getOriginalFilename().split("\\.");
        FileInfo fileInfo = FileInfo.newBuilder()
            .setFileExtention(chunkFileName[chunkFileName.length-1])
            .setFileType(file.getContentType())
            .setFileName(file.getOriginalFilename())
            .setBucket(bucket)
            .build();

        UploadFileRequest request =  UploadFileRequest.newBuilder()
            .setFileInfo(fileInfo)
            .build();

        
        streamObserver.onNext(request);

        // upload file as chunk

        byte[] bytes = new byte[4096];
        int size;

        

        try {
            InputStream inputStream = file.getInputStream();
            while((size = inputStream.read(bytes)) > 0){
                UploadFileRequest fileRequest =  UploadFileRequest.newBuilder()
                .setData(ByteString.copyFrom(bytes, 0, size))
                .build();

                streamObserver.onNext(fileRequest);
            }

            // close the stream
            inputStream.close();
            streamObserver.onCompleted();
        } catch (IOException e) {
            e.printStackTrace();
        }



        return "";
    }

}

class fileUploadObserver implements StreamObserver<UploadFileResponse> {

    @Override
    public void onNext(UploadFileResponse response) {
        System.out.println(
                "File upload status :: " + response.getStatus()
        );
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onCompleted() {

    }

}
