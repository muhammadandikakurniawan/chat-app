package app.storage_service.delivery.grpc.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import com.google.protobuf.ByteString;

import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import app.storage_service.delivery.grpc.middleware.AppKeyMiddleware;
import app.storage_service.grpc.chat_service.*;
import app.storage_service.grpc.chat_service.ChatServiceGrpc.ChatServiceImplBase;
import app.storage_service.grpc.chat_service.UploadFileRequest.FileInfo;
import app.storage_service.helper.MinioHelper;
import io.grpc.stub.StreamObserver;
import io.minio.ObjectWriteResponse;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@GRpcService(applyGlobalInterceptors = true, interceptors = {AppKeyMiddleware.class})
public class ChatServiceGrpcHandler extends ChatServiceImplBase{

    @Autowired
    private MinioHelper _minioHelper;

    private static final Path SERVER_BASE_PATH = Paths.get("src/main/resources/output");
    
    private OutputStream getFilePath(UploadFileRequest request) throws IOException {
        var fileName = request.getFileInfo().getFileName();
        return Files.newOutputStream(SERVER_BASE_PATH.resolve(fileName), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }

    private void writeFile(OutputStream writer, ByteString content) throws IOException {
        writer.write(content.toByteArray());
        writer.flush();
    }

    private void closeFile(OutputStream writer){
        try {
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public StreamObserver<UploadFileRequest> uploadFile(StreamObserver<UploadFileResponse> responseObserver){
        return new StreamObserver<UploadFileRequest>(){

            OutputStream writer;
            String status = "success";
            FileInfo fileInfo;
            byte[] buffer = new byte[]{};

            @Override
            public void onNext(UploadFileRequest req) {
                
                if(req.hasFileInfo()){
                    fileInfo = req.getFileInfo();
                }else{
                    byte[] reqBuffer = req.getData().toByteArray();
                    byte[] mergeBuffer = new byte[buffer.length+reqBuffer.length];
                    System.arraycopy(buffer, 0, mergeBuffer, 0, buffer.length);
                    System.arraycopy(reqBuffer, 0, mergeBuffer, buffer.length, reqBuffer.length);
                    buffer = mergeBuffer;
                }

            }
            @Override
            public void onError(Throwable t) {
                status = "failed";
                
            }

            @Override
            public void onCompleted() {
                var fileName = fileInfo.getFileName();
                String filePath = SERVER_BASE_PATH.resolve(fileName).toFile().getAbsolutePath();
                System.out.println(filePath);
                try {
                    ObjectWriteResponse uploadMinioRes = _minioHelper.uploadFile(fileInfo.getBucket(), buffer, fileInfo.getFileName());
                    String etag = uploadMinioRes.etag();
                    System.out.println(etag);
                } catch (InvalidKeyException | ErrorResponseException | InsufficientDataException | InternalException
                        | InvalidResponseException | NoSuchAlgorithmException | ServerException | XmlParserException
                        | IllegalArgumentException | IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                status = "success";

                UploadFileResponse response = UploadFileResponse.newBuilder()
                .setStatus("200")
                .build();

                responseObserver.onNext(response);
                responseObserver.onCompleted();
            }

           
        };

    }

    // @Override
    // public StreamObserver<UploadFileRequest> uploadFile(StreamObserver<UploadFileResponse> responseObserver){
    //     return new StreamObserver<UploadFileRequest>(){

    //         OutputStream writer;
    //         String status = "success";
    //         FileInfo fileInfo;

    //         @Override
    //         public void onNext(UploadFileRequest req) {
                
    //             try{

    //                 if(req.hasFileInfo()){
    //                     fileInfo = req.getFileInfo();
    //                     writer = getFilePath(req);
    //                 }else{
    //                     writeFile(writer, req.getData());
    //                 }

    //             }catch(IOException e){
                    
    //                 this.onError(e);

    //             }

    //         }
    //         @Override
    //         public void onError(Throwable t) {
    //             status = "failed";
                
    //         }
    //         @Override
    //         public void onCompleted() {
    //             var fileName = fileInfo.getFileName();
    //             String filePath = SERVER_BASE_PATH.resolve(fileName).toFile().getAbsolutePath();
    //             System.out.println(filePath);
    //             try {
    //                 ObjectWriteResponse uploadMinioRes = _minioHelper.uploadFile(fileInfo.getBucket(), filePath, fileInfo.getFileName());
    //                 String etag = uploadMinioRes.etag();
    //                 System.out.println(etag);
    //             } catch (InvalidKeyException | ErrorResponseException | InsufficientDataException | InternalException
    //                     | InvalidResponseException | NoSuchAlgorithmException | ServerException | XmlParserException
    //                     | IllegalArgumentException | IOException e) {
    //                 // TODO Auto-generated catch block
    //                 e.printStackTrace();
    //             }

    //             closeFile(writer);

    //             status = "in progress";

    //             UploadFileResponse response = UploadFileResponse.newBuilder()
    //             .setStatus("200")
    //             .build();

    //             responseObserver.onNext(response);
    //             responseObserver.onCompleted();
    //         }

           
    //     };

    // }

    // @Override
    // public StreamObserver<UploadFileResponse> uploadFile(StreamObserver<UploadFileRequest> request) {
    //     String apiKey = (String) AppKeyMiddleware.API_KEY.get();
    //     // UploadFileResponse.Builder response = UploadFileResponse.newBuilder();
    //     // response.setStatus("okokook");

    //     // responseObserver.onNext(response.build());
    //     // responseObserver.onCompleted();

    //     return new StreamObserver<UploadFileRequest>(){
    //         @Override
    //         public void onNext(FileUploadRequest fileUploadRequest) {
    //             try{
    //                 if(fileUploadRequest.hasMetadata()){
    //                     writer = getFilePath(fileUploadRequest);
    //                 }else{
    //                     writeFile(writer, fileUploadRequest.getFile().getContent());
    //                 }
    //             }catch (IOException e){
    //                 this.onError(e);
    //             }
    //         }

    //         @Override
    //         public void onError(Throwable throwable) {
    //             status = Status.FAILED;
    //             this.onCompleted();
    //         }

    //         @Override
    //         public void onCompleted() {
    //             closeFile(writer);
    //             status = Status.IN_PROGRESS.equals(status) ? Status.SUCCESS : status;
    //             FileUploadResponse response = FileUploadResponse.newBuilder()
    //                     .setStatus(status)
    //                     .build();
    //             responseObserver.onNext(response);
    //             responseObserver.onCompleted();
    //         }
    //     };
    // }
}
