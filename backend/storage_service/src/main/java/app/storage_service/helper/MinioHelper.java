package app.storage_service.helper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.minio.BucketExistsArgs;
import io.minio.GetObjectArgs;
import io.minio.GetObjectResponse;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import io.minio.SetBucketPolicyArgs;
import io.minio.UploadObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@Component
public class MinioHelper {
    
    @Autowired
	private MinioClient _minioCLient;

    public void makeBucket(String bucketName) throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IllegalArgumentException, IOException{
        // create bucket 
        _minioCLient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());

        // Set bucket strategy
        String policyJson =
        "{\n" +
                "     \"Statement\": [\n" +
                "         {\n" +
                "             \"Action\": [\n" +
                "                 \"s3:GetBucketLocation\",\n" +
                "                 \"s3:ListBucket\"\n" +
                "             ],\n" +
                "             \"Effect\": \"Allow\",\n" +
                "             \"Principal\": \"*\",\n" +
                "             \"Resource\": \"arn:aws:s3:::"+bucketName+"\"\n" +
                "         },\n" +
                "         {\n" +
                "             \"Action\": [\n" +
                "                 \"s3:GetObject\",\n" +
                "                 \"s3:PutObject\"\n" +
                "             ],\n" +
                "             \"Effect\": \"Allow\",\n" +
                "             \"Principal\": \"*\",\n" +
                "             \"Resource\": \"arn:aws:s3:::"+bucketName+"/*\"\n" +
                "         }\n" +
                "     ],\n" +
                "     \"Version\": \"2012-10-17\"\n" +
                " }";
        
        _minioCLient.setBucketPolicy(
            SetBucketPolicyArgs.builder()
            .bucket(bucketName)
            .config(policyJson)
            .build()
        );
    }

    public ObjectWriteResponse uploadFile(String bucket, String file, String objectName) throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IllegalArgumentException, IOException{

        // check bucket is exists, create new if false  

        boolean isBucketExists = _minioCLient.bucketExists(
            BucketExistsArgs.builder()
            .bucket(bucket)
            .build()
        );

        if(!isBucketExists){
            this.makeBucket(bucket);
        }

        return _minioCLient.uploadObject(
            UploadObjectArgs.builder()
            .bucket(bucket)
            .object(objectName)
            .filename(file)
            .build()
        );
    }

    public ObjectWriteResponse uploadFile(String bucket, byte[] buffer, String objectName) throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException{
        
        boolean isBucketExists = _minioCLient.bucketExists(
            BucketExistsArgs.builder()
            .bucket(bucket)
            .build()
        );

        if(!isBucketExists){
            this.makeBucket(bucket);
        }
        
        long chunk = 5 * 1024 * 1024;
        InputStream stream = new ByteArrayInputStream(buffer);
        PutObjectArgs args = PutObjectArgs.builder()
        .bucket(bucket)
        .object(objectName)
        .contentType("application/octet-stream")
        .stream(stream, buffer.length, chunk)
        .build();

        return _minioCLient.putObject(args);
    }

    public GetObjectResponse getFile(String bucket, String objectName) throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException{
        GetObjectArgs args = GetObjectArgs.builder()
        .bucket(bucket)
        .object(objectName)
        .build();
        return _minioCLient.getObject(args);
    }

}
