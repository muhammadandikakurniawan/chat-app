package app.storage_service.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import app.storage_service.grpc.chat_service.ChatServiceGrpc;
import app.storage_service.grpc.chat_service.ChatServiceGrpc.ChatServiceBlockingStub;
import app.storage_service.grpc.chat_service.ChatServiceGrpc.ChatServiceStub;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

@Configuration
public class GrpcConfiguration {
    
    @Bean
    public ChatServiceBlockingStub getChatServiceGrpcBlockingStub(){
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 4042).usePlaintext().build();
        ChatServiceBlockingStub client = ChatServiceGrpc.newBlockingStub(channel);

        return client;
    }

    @Bean
    public ChatServiceStub getChatServiceGrpcStub(){
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 4042).usePlaintext().build();
        ChatServiceStub client = ChatServiceGrpc.newStub(channel);

        return client;
    }

}
