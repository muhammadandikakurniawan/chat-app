package app.storage_service.configuration;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import io.minio.BucketExistsArgs;
import io.minio.MinioClient;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@Configuration()
public class MinioConfiguration {

    @Autowired
    private Environment env;
    
    @Bean
    public MinioClient setMinioClient() throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IllegalArgumentException, IOException{
        String host = env.getProperty("minio.host");
        String accessKey = env.getProperty("minio.access_key");
        String secretKey = env.getProperty("minio.secret_key");
        MinioClient minioClient =  MinioClient.builder().endpoint(host).credentials(accessKey, secretKey) .build();
        minioClient.bucketExists(BucketExistsArgs.builder().bucket("test-bucket").build());
        return minioClient;
    }

}
