package app.storage_service;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import app.storage_service.helper.MinioHelper;
import io.minio.BucketArgs;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@SpringBootTest
public class MinioTest {
    
 
    @Autowired
	private MinioHelper _minioHelper;

	@Test
	void makeBucketTest() throws InvalidKeyException, ErrorResponseException, InsufficientDataException, InternalException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IllegalArgumentException, IOException {
		// _minioHelper.makeBucket("test-minio-helper");
	}
    
	@Test
	void appendArray(){
		byte[] a1 = new byte[]{};
		byte[] a2 = new byte[]{4,5,6,8,9};

		byte[] res = new byte[a1.length + a2.length];
		System.arraycopy(a1, 0, res, 0, a1.length);
		System.arraycopy(a2, 0, res, a1.length, a2.length);

		a1 = res;
		System.out.println(a1);
	}
}
