﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RealtimeService.Repository.DBContext;
using RealtimeService.Repository.Impl;
using RealtimeService.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RealtimeService.Grpc.Interceptors;
using RealtimeService.MongoDbRepository.DbContext;
using RealtimeService.MongoDbRepository.Interfaces;
using RealtimeService.MongoDbRepository.Impl;
using Microsoft.AspNetCore.SignalR;
using RealtimeService.Grpc.SignalRHubs;
using RealtimeService.Grpc.SignalRFilters;

namespace RealtimeService.Grpc
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //configure database
            string conStr = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<RealtimeServiceDbContext>(options => options.UseNpgsql(conStr));
            services.AddIdentity<ApplicationUser, IdentityRole>()
                // .AddSignInManager<ApplicationUser>()
                .AddEntityFrameworkStores<RealtimeServiceDbContext>()
                .AddDefaultTokenProviders();

            string mongoConnStr = Configuration.GetConnectionString("MongoConnection");
            services.AddSingleton<MongoDbContext>(new MongoDbContext(mongoConnStr));

            //repositories
            services.AddScoped<IChatRoomMemberRepository, ChatRoomMemberRepository>();
            services.AddScoped<IChatRoomMessageRepository, ChatRoomMessageRepository>();
            services.AddScoped<IChatRoomRepository, ChatRoomRepository>();

            services.AddCors(config => {
                config.AddPolicy("AllowAll",policy => {
                    policy.
                    AllowCredentials().
                    AllowAnyHeader().
                    AllowAnyMethod();
                });
            });
            services.AddCors(o => o.AddPolicy("AllowAllGrpc", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding","custom-header-1","grpc-status","grpc-message");
            }));

            services.AddSignalR(opt => {
                
            })
            .AddHubOptions<ChatHub>(opt => {
                opt.AddFilter<AppKeySignalRFilter>();
            });

            services.AddGrpc().AddServiceOptions<ChatServiceHandler>(
                opt => {
                    opt.Interceptors.Add<AppKeyInterceptor>();
                }
            );
            services.AddGrpcReflection();

            // services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseGrpcWeb(new GrpcWebOptions{DefaultEnabled=true});
            app.UseCors("AllowAll");
            app.UseEndpoints(endpoints =>
            {
               
                // endpoints.MapGrpcService<GreeterService>();
                // endpoints.MapGrpcService<ChatServiceHandler>();

                endpoints.MapGrpcService<GreeterService>().EnableGrpcWeb();
                endpoints.MapGrpcService<ChatServiceHandler>().EnableGrpcWeb();

                // endpoints.MapGrpcService<GreeterService>().EnableGrpcWeb().RequireCors("AllowAllGrpc");
                // endpoints.MapGrpcService<ChatServiceHandler>().EnableGrpcWeb().RequireCors("AllowAllGrpc");

                endpoints.MapHub<ChatHub>("/hub/chat");
                
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
                if (env.IsDevelopment())
                {
                    endpoints.MapGrpcReflectionService();
                }
            });
        }
    }
}
