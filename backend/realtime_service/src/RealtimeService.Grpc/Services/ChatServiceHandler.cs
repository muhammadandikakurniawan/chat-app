using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Grpc.Core;
using RealtimeService.Repository.Interfaces;
using RealtimeService.Repository.Entities;
using RealtimeService.MongoDbRepository.Interfaces;
using RealtimeService.MongoDbRepository.Models;
using RealtimeService.Grpc.Interceptors;
using Microsoft.AspNetCore.SignalR;
using RealtimeService.Grpc.SignalRHubs;

namespace RealtimeService.Grpc
{
    public class ChatServiceHandler : ChatService.ChatServiceBase
    {
        private readonly ILogger<ChatServiceHandler> _logger;
        private readonly IChatRoomRepository _chatRoomRepository;
        private readonly IChatRoomMemberRepository _chatRoomMemberRepository;
        private readonly IChatRoomMessageRepository  _chatRoomMessageRepository ;
        private readonly IHubContext<ChatHub> _chatHub;

        public ChatServiceHandler(
            ILogger<ChatServiceHandler> logger
            ,IChatRoomRepository chatRoomRepository
            ,IChatRoomMemberRepository chatRoomMemberRepository
            ,IChatRoomMessageRepository  chatRoomMessageRepository 
            ,IHubContext<ChatHub> chatHub
        )
        {
            _logger = logger;
            _chatRoomRepository = chatRoomRepository;
            _chatRoomMemberRepository = chatRoomMemberRepository;
            _chatRoomMessageRepository = chatRoomMessageRepository;
            _chatHub = chatHub;
        }

        public override async Task<CreateRoomResponse> CreateRoom(CreateRoomRequest request, ServerCallContext context)
        {
            CreateRoomResponse response = new CreateRoomResponse();
            response.Success = true;
            response.StatusCode = "200";
            try{

                ChatRoomEntity chatRoom = new ChatRoomEntity();
                chatRoom.Name = request.Name;
                chatRoom.CreatorUserID = request.CreatorUserId;
                chatRoom.Code = Guid.NewGuid().ToString("N").Substring(0,7);

                var repoRes = _chatRoomRepository.Create(chatRoom);
                if(!repoRes.IsSuccess){
                    response.Success = false;
                    response.Message = repoRes.Message;
                    response.StatusCode = "510";
                    return response;
                }

                ChatRoomMember chatRoomMemberData = new ChatRoomMember();
                chatRoomMemberData.RoomID = repoRes.Data.ID;
                chatRoomMemberData.MembersUserId = new List<string>(){
                    chatRoom.CreatorUserID,
                };
                var createRoomMemberRes = await _chatRoomMemberRepository.CreateAsync(chatRoomMemberData);
                if(!createRoomMemberRes.IsSuccess){
                    response.Success = false;
                    response.Message = createRoomMemberRes.Message;
                    response.StatusCode = "511";
                    return response;
                }

                var chatMessageData = new RealtimeService.MongoDbRepository.Models.ChatRoomMessage();
                chatMessageData.RoomID = repoRes.Data.ID;
                chatMessageData.Messages = new List<RealtimeService.MongoDbRepository.Models.MessageModel>();
                var createMessageRes = await _chatRoomMessageRepository.CreateAsync(chatMessageData);
                if(!createMessageRes.IsSuccess){
                    response.Success = false;
                    response.Message = createMessageRes.Message;
                    response.StatusCode = "512";
                    return response;
                }

                response.Data = new ChatRoomModel{
                    Id = chatRoom.ID,
                    Code = chatRoom.Code,
                    Name = chatRoom.Name,
                    CreatorUserId = chatRoom.CreatorUserID
                };

            }catch(Exception ex){
                response.Success = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                response.StatusCode = "500";
            }

            return response;
        }
       
        public override async Task<JoinRoomResponse> JoinRoom(JoinRoomRequest request, ServerCallContext context){
            JoinRoomResponse response = new JoinRoomResponse();
            response.Success = true;
            response.StatusCode = "200";

            try{

                var roomData = _chatRoomRepository.GetByCode(request.RoomCode);
                if(roomData.Data == null){
                    response.Success = false;
                    response.Message = "room not found";
                    response.StatusCode = "404";
                    return response;
                }
                var chatRoom = roomData.Data;
                

                var chatRoomMemberData = await _chatRoomMemberRepository.GetByRoomIdAndMemberId(chatRoom.ID, request.MemberUserId);
                if(chatRoomMemberData.Data != null){
                    response.Message = "success";

                    response.Data = new ChatRoomModel{
                        Id = chatRoom.ID,
                        Code = chatRoom.Code,
                        Name = chatRoom.Name,
                        CreatorUserId = chatRoom.CreatorUserID
                    };
                    return response;
                }
                chatRoomMemberData = await _chatRoomMemberRepository.GetByRoomIdAsync(chatRoom.ID);
                chatRoomMemberData.Data.MembersUserId.Add(request.MemberUserId);
                var createRoomMemberRes = await _chatRoomMemberRepository.UpdateMemberAsync(chatRoom.ID, chatRoomMemberData.Data.MembersUserId.ToArray());
                if(!createRoomMemberRes.IsSuccess){
                    response.Success = false;
                    response.Message = createRoomMemberRes.Message;
                    response.StatusCode = "512";
                    return response;
                }
                response.Message = "success";

                response.Data = new ChatRoomModel{
                    Id = chatRoom.ID,
                    Code = chatRoom.Code,
                    Name = chatRoom.Name,
                    CreatorUserId = chatRoom.CreatorUserID
                };
            }catch(Exception ex){
                response.Success = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                response.StatusCode = "500";
            }

            return response;
        }

        public override async Task<SendMessageResponse> SendMessage(SendMessageRequest request, ServerCallContext context){
            SendMessageResponse response = new SendMessageResponse();
            response.Success = true;
            response.StatusCode = "200";

            try{

                var messageData = new RealtimeService.MongoDbRepository.Models.MessageModel(){
                    UserID = request.UserId,
                    Message = request.Message,
                    CreatedAt = DateTime.Now
                };
                
                var roomMessageData =  await _chatRoomMessageRepository.GetMessagePaginationAsync(request.RoomId, -1, 1);
                if(!roomMessageData.IsSuccess){
                    response.Success = false;
                    response.Message = roomMessageData.Message;
                    response.StatusCode = "510";
                    return response;
                }
                if(roomMessageData.Data == null){
                    response.Success = false;
                    response.Message = "data not found";
                    response.StatusCode = "404";
                    return response;
                }

                var res = await _chatRoomMessageRepository.AddMessageByRoomIdAsync(request.RoomId, messageData);
                roomMessageData =  await _chatRoomMessageRepository.GetMessagePaginationAsync(request.RoomId, -1, 1);
                if(!roomMessageData.IsSuccess){
                    response.Success = false;
                    response.Message = roomMessageData.Message;
                    response.StatusCode = "510";
                    return response;
                }
                if(roomMessageData.Data == null){
                    response.Success = false;
                    response.Message = "data not found";
                    response.StatusCode = "404";
                    return response;
                }
                
                var messages = roomMessageData.Data.Messages.Select(m => new MessageModel{
                    UserId = m.UserID,
                    Message = m.Message,
                    CreatedAt = m.CreatedAt.ToString()
                });
                
                response.Data = new ChatRoomMessage(){
                    RoomId = roomMessageData.Data.RoomID
                };
                response.Data.Messages.AddRange(messages);

                await _chatHub.Clients.Group(request.RoomId).SendAsync("receiveRoomMessage", request.UserId, request.Message, messageData.CreatedAt.ToString());

            }catch(Exception ex){
                response.Success = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                response.StatusCode = "500";
            }
            return response;
        }
    }
}