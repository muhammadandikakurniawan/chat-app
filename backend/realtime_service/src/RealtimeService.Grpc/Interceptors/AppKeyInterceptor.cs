using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;

namespace RealtimeService.Grpc.Interceptors
{
    public class AppKeyInterceptor : Interceptor
    {
        
        public override Task<TResponse> UnaryServerHandler<TRequest, TResponse>(
        TRequest request,
        ServerCallContext context,
        UnaryServerMethod<TRequest, TResponse> continuation){
            List<Metadata.Entry> metas = context.RequestHeaders.ToList();
            Metadata.Entry appKey = context.RequestHeaders.FirstOrDefault(k => k.Key.Equals("x-api-key"));
            if(appKey == null){
                throw new Exception("unauthorize"); 
            }

            return continuation(request, context);
        }

    }
}