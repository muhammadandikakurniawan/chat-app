using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace RealtimeService.MongoDbRepository.Models
{
    public class ChatRoomMember
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        [BsonElement("room_id")]
        public string RoomID {get;set;}

        [BsonElement("membes_user_id")]
        public List<string> MembersUserId {get;set;}
    }
}