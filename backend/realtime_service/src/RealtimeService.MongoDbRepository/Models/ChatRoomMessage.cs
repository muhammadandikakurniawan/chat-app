using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace RealtimeService.MongoDbRepository.Models
{
    public class ChatRoomMessage
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        [BsonElement("room_id")]
        public string RoomID {get;set;}

        [BsonElement("messages")]
        public List<MessageModel> Messages {get;set;}
        

    }

    public class MessageModel {
        [BsonElement("user_id")]
        public string UserID {get;set;}

        [BsonElement("message")]
        public string Message {get;set;}

        [BsonElement("created_at")]
        public DateTime CreatedAt {get;set;}

        [BsonElement("updated_at")]
        public DateTime? UpdatedAt {get;set;}

        [BsonElement("deleted_at")]
        public DateTime? DeletedAt {get;set;}
    }
}