using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RealtimeService.MongoDbRepository.Models;
using MongoDB.Driver;

namespace RealtimeService.MongoDbRepository.DbContext
{
    public class MongoDbContext
    {
        public IMongoCollection<ChatRoomMember> ChatRoomMember {get;set;}
        public IMongoCollection<ChatRoomMessage> ChatRoomMessage {get;set;}

        public MongoDbContext(string connString){
            var client = new MongoClient(connString);
            var db = client.GetDatabase("realtime_service");
            
            // === register collections ===
            ChatRoomMember = db.GetCollection<ChatRoomMember>("chat_room_members");
            ChatRoomMessage = db.GetCollection<ChatRoomMessage>("chat_room_messages");
        }
       
    }
}