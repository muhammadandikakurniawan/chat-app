using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealtimeService.MongoDbRepository.DbContext
{
    public class RepositoryResponse<T>
    {
        public bool IsSuccess {get;set;}
        public string Message {get;set;}
        public T Data {get;set;}
    }
}