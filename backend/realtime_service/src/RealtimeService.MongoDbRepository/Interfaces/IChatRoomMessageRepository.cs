using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RealtimeService.MongoDbRepository.DbContext;
using RealtimeService.MongoDbRepository.Models;

namespace RealtimeService.MongoDbRepository.Interfaces
{
    public interface IChatRoomMessageRepository
    {
        Task<RepositoryResponse<ChatRoomMessage>> CreateAsync(ChatRoomMessage data);
        Task<RepositoryResponse<ChatRoomMessage>> AddMessageByRoomIdAsync(string roomId, MessageModel data);
        Task<RepositoryResponse<ChatRoomMessage>> GetMessagePaginationAsync(string roomId, int start, int? limit);
    }
}