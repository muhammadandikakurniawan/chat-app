using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RealtimeService.MongoDbRepository.DbContext;
using RealtimeService.MongoDbRepository.Models;

namespace RealtimeService.MongoDbRepository.Interfaces
{
    public interface IChatRoomMemberRepository
    {
        Task<RepositoryResponse<ChatRoomMember>> CreateAsync(ChatRoomMember data);
        Task<RepositoryResponse<ChatRoomMember>> UpdateMemberAsync(string roomId, string[] membersId);
        Task<RepositoryResponse<ChatRoomMember>> GetByRoomIdAsync(string roomId);
        Task<RepositoryResponse<ChatRoomMember>> GetByRoomIdAndMemberId(string roomId, string memberid);
    }
}