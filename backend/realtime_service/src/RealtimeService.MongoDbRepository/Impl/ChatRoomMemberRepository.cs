using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RealtimeService.MongoDbRepository.DbContext;
using RealtimeService.MongoDbRepository.Models;
using RealtimeService.MongoDbRepository.Interfaces;
using MongoDB.Driver;

namespace RealtimeService.MongoDbRepository.Impl
{
    public class ChatRoomMemberRepository : IChatRoomMemberRepository
    {
        private IMongoCollection<ChatRoomMember> _collection {get;set;}
        public ChatRoomMemberRepository(MongoDbContext dbCtx){
            _collection = dbCtx.ChatRoomMember;
        }

        public async Task<RepositoryResponse<ChatRoomMember>> CreateAsync(ChatRoomMember data){
            RepositoryResponse<ChatRoomMember> response = new RepositoryResponse<ChatRoomMember>();
            try{
                await _collection.InsertOneAsync(data);
                response.IsSuccess = true;
            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }

        public async Task<RepositoryResponse<ChatRoomMember>> UpdateMemberAsync(string roomId, string[] membersId){
            RepositoryResponse<ChatRoomMember> response = new RepositoryResponse<ChatRoomMember>();
            try{
                UpdateDefinition<ChatRoomMember> updateDef = Builders<ChatRoomMember>.Update.Set(r => r.MembersUserId, membersId.ToList());
                UpdateResult res = await _collection.UpdateOneAsync(r => r.RoomID.Equals(roomId), updateDef);

                response.IsSuccess = true;
            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }

        public async Task<RepositoryResponse<ChatRoomMember>> GetByRoomIdAsync(string roomId){
            RepositoryResponse<ChatRoomMember> response = new RepositoryResponse<ChatRoomMember>();
            try{
                response.Data = await _collection.Find(r => r.RoomID.Equals(roomId)).FirstAsync();
                response.IsSuccess = true;
            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }

        public async Task<RepositoryResponse<ChatRoomMember>> GetByRoomIdAndMemberId(string roomId, string memberid){
            RepositoryResponse<ChatRoomMember> response = new RepositoryResponse<ChatRoomMember>();
            try{
                response.Data = await _collection.Find(r => r.RoomID.Equals(roomId) && r.MembersUserId.Contains(memberid)).FirstAsync();
                response.IsSuccess = true;
            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }

    }
}