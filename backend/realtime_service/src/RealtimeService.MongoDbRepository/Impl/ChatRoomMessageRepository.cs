using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RealtimeService.MongoDbRepository.DbContext;
using RealtimeService.MongoDbRepository.Models;
using RealtimeService.MongoDbRepository.Interfaces;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Bson;

namespace RealtimeService.MongoDbRepository.Impl
{
    public class ChatRoomMessageRepository : IChatRoomMessageRepository
    {
        
        private IMongoCollection<ChatRoomMessage> _collection {get;set;}
        public ChatRoomMessageRepository(MongoDbContext dbCtx){
            _collection = dbCtx.ChatRoomMessage;
        }

        public async Task<RepositoryResponse<ChatRoomMessage>> CreateAsync(ChatRoomMessage data){
            RepositoryResponse<ChatRoomMessage> response = new RepositoryResponse<ChatRoomMessage>();
            try{
                await _collection.InsertOneAsync(data);
                response.IsSuccess = true;
            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }
        public async Task<RepositoryResponse<ChatRoomMessage>> AddMessageByRoomIdAsync(string roomId, MessageModel dataMessage){
            RepositoryResponse<ChatRoomMessage> response = new RepositoryResponse<ChatRoomMessage>();
            try{
                FilterDefinition<ChatRoomMessage> filter = Builders<ChatRoomMessage>.Filter.Eq(r => r.RoomID, roomId);
                UpdateDefinition<ChatRoomMessage> updateDef = Builders<ChatRoomMessage>.Update.Push<MessageModel>(e => e.Messages, dataMessage);
                response.Data = await _collection.FindOneAndUpdateAsync(filter, updateDef);
                response.IsSuccess = true;
            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }

        public async Task<RepositoryResponse<ChatRoomMessage>> GetMessagePaginationAsync(string roomId, int start, int? limit){
            RepositoryResponse<ChatRoomMessage> response = new RepositoryResponse<ChatRoomMessage>();
            try{
                FilterDefinition<ChatRoomMessage> filter = Builders<ChatRoomMessage>.Filter.And(
                    Builders<ChatRoomMessage>.Filter.Eq(x => x.RoomID, roomId)
                );

                var fieldsBuilder = Builders<ChatRoomMessage>.Projection;
                var fields = fieldsBuilder.Slice(x => x.Messages, start, limit);
                response.Data = await _collection.Find(filter).Project<ChatRoomMessage>(fields).FirstAsync();
           
                // var _projection = Builders<ChatRoomMessage>.Projection;
                // response.Data = await _collection.Find(filter, FindOptions(s => s.)).Project<ChatRoomMessage>(_projection.Include(x => x.RoomID).Include(x => x.Messages).Slice(x => x.Messages, start, limit)).FirstAsync<ChatRoomMessage>();

                response.IsSuccess = true;
            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }
    }
}