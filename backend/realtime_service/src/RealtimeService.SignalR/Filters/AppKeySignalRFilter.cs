using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;  
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace RealtimeService.SignalR.Filters
{
    public class AppKeySignalRFilter : IHubFilter
    {
        public async ValueTask<object> InvokeMethodAsync(HubInvocationContext invocationContext, Func<HubInvocationContext, ValueTask<object>> next)
        {
            Console.WriteLine($"Calling hub method '{invocationContext.HubMethodName}'");
            try
            {
                return await next(invocationContext);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception calling '{invocationContext.HubMethodName}': {ex}");
                throw;
            }
        }

        // Optional method
        public Task OnConnectedAsync(HubLifetimeContext context, Func<HubLifetimeContext, Task> next)
        {
            HttpContext httpCtx = context.Context.GetHttpContext();
            StringValues appKey;

            var qAppKey = httpCtx.Request.Query["x-app-key"];

            if(!httpCtx.Request.Headers.TryGetValue("x-app-key", out appKey) && qAppKey.Count <= 0 ){
                // Client(context.Context.ConnectionId)
               context.Hub.Context.Abort();
            }
            return next(context);
        }

        // Optional method
        public Task OnDisconnectedAsync(
            HubLifetimeContext context, Exception exception, Func<HubLifetimeContext, Exception, Task> next)
        {
            return next(context, exception);
        }
    }
}