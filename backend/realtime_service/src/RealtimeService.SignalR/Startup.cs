using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.SignalR;
using RealtimeService.SignalR.Hubs;
using RealtimeService.SignalR.Filters;
using RealtimeService.Repository.DBContext;
using RealtimeService.Repository.Impl;
using RealtimeService.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RealtimeService.MongoDbRepository.DbContext;
using RealtimeService.MongoDbRepository.Interfaces;
using RealtimeService.MongoDbRepository.Impl;


namespace RealtimeService.SignalR
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //configure database
            string conStr = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<RealtimeServiceDbContext>(options => options.UseNpgsql(conStr));


            services.AddIdentity<ApplicationUser, IdentityRole>()
                // .AddSignInManager<ApplicationUser>()
                .AddEntityFrameworkStores<RealtimeServiceDbContext>()
                .AddDefaultTokenProviders();

            string mongoConnStr = Configuration.GetConnectionString("MongoConnection");
            services.AddSingleton<MongoDbContext>(new MongoDbContext(mongoConnStr));

            //repositories
            services.AddScoped<IChatRoomMemberRepository, ChatRoomMemberRepository>();
            services.AddScoped<IChatRoomMessageRepository, ChatRoomMessageRepository>();
            services.AddScoped<IChatRoomRepository, ChatRoomRepository>();


            services.AddControllers();
            services.AddControllersWithViews();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RealtimeService.SignalR", Version = "v1" });
            });

            services.AddSignalR(opt => {

            })
            .AddHubOptions<ChatHub>(opt => {
                opt.AddFilter<AppKeySignalRFilter>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RealtimeService.SignalR v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>("/hub/chat");
                endpoints.MapControllers();
            });
        }
    }
}
