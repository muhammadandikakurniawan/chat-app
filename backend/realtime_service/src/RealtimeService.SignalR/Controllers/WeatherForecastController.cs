﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RealtimeService.Repository.Interfaces;
using RealtimeService.Repository.Entities;

namespace RealtimeService.SignalR.Controllers
{
    [ApiController]
    [Route("init")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };
        private readonly ILogger<WeatherForecastController> _logger;

        private readonly IChatRoomRepository _chatRoomRepository;
        public WeatherForecastController(ILogger<WeatherForecastController> logger, IChatRoomRepository chatRoomRepository)
        {
            _logger = logger;
            _chatRoomRepository = chatRoomRepository;
        }


        [HttpGet]
        [Route("create-room")]
        public Object CreateChatRoom(){
            ChatRoomEntity data = new ChatRoomEntity();
            data.ID = Guid.NewGuid().ToString();
            data.Name = "asdasdasd";
            var res = _chatRoomRepository.Create(data);
            return res;
        }       


        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
