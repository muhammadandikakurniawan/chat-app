using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;  
using RealtimeService.Repository.Interfaces;
using RealtimeService.MongoDbRepository.Interfaces;
using RealtimeService.MongoDbRepository.Models;
using RealtimeService.SignalR.Filters;

namespace RealtimeService.SignalR.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IChatRoomRepository _chatRoomRepository;
        private readonly IChatRoomMessageRepository  _chatRoomMessageRepository ;

        public ChatHub(
            IChatRoomRepository chatRoomRepository
            ,IChatRoomMessageRepository  chatRoomMessageRepository 
        ){
            _chatRoomRepository = chatRoomRepository;
            _chatRoomMessageRepository = chatRoomMessageRepository;
        }

        public override Task OnConnectedAsync()
        {
            String id = this.Context.ConnectionId;
            this.Clients.Client(this.Context.ConnectionId).SendAsync("connected",id);
            return base.OnConnectedAsync();
        }
        

        public override Task OnDisconnectedAsync(Exception exception)
        {
            return base.OnDisconnectedAsync(exception);
        }

        [HubMethodName("joinChatRoom")]
        public async Task JoinChatRoom(string roomId, string guestUserId, string email, string groupName){
            await this.Groups.AddToGroupAsync(guestUserId, roomId);
            
            await this.Clients.Group(roomId).SendAsync("joinedRoom", $"{email} has joined the group {groupName}.");
        }
        
        [HubMethodName("sendChatRoomMessage")]
        public async Task SendChatRoomMessage(string roomId, string userId, string message)  
        {  

            var messageData = new MessageModel{
                UserID = userId,
                Message = message,
                CreatedAt = DateTime.Now
            };

            var insertMsgRes = await _chatRoomMessageRepository.AddMessageByRoomIdAsync(roomId, messageData);
            if(!insertMsgRes.IsSuccess){

            }

            // await this.Clients.Group(roomId).SendAsync("receiveMessageRoom", $"{email} has joined the group {groupName}.");

            // await this.Clients.Client(this.Context.ConnectionId).SendAsync("","");
            // await Clients.All.SendAsync("receiveMessage", user, message);  
        }  
    }
}