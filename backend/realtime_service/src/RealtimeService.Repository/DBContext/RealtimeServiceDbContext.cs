﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using RealtimeService.Repository.Entities;


namespace RealtimeService.Repository.DBContext
{
    public partial class RealtimeServiceDbContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<ChatRoomEntity> ChatRoom { get; set; }
        public RealtimeServiceDbContext()
        {
        }

        public RealtimeServiceDbContext(DbContextOptions<RealtimeServiceDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
