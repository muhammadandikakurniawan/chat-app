using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RealtimeService.Repository.DBContext;
using RealtimeService.Repository.Entities;

namespace RealtimeService.Repository.Interfaces
{
    public interface IChatRoomRepository
    {
        RepositoryResponse<ChatRoomEntity> Create(ChatRoomEntity data);
        RepositoryResponse<ChatRoomEntity> GetByCode(string code);
    }
}