using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RealtimeService.Repository.Interfaces;
using RealtimeService.Repository.Entities;
using RealtimeService.Repository.DBContext;

namespace RealtimeService.Repository.Impl
{
    public class ChatRoomRepository : IChatRoomRepository
    {
        private readonly RealtimeServiceDbContext _dbContext;
        public ChatRoomRepository(RealtimeServiceDbContext dbContext){
            _dbContext = dbContext;
        }

        public RepositoryResponse<ChatRoomEntity> Create(ChatRoomEntity data){
            RepositoryResponse<ChatRoomEntity> response = new RepositoryResponse<ChatRoomEntity>();
            try{
                response.IsSuccess = true;
                data.ID = Guid.NewGuid().ToString();
                this._dbContext.ChatRoom.Add(data);
                this._dbContext.SaveChanges();
                response.Data = data;

            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }

        public RepositoryResponse<ChatRoomEntity?> GetByCode(string code){
            RepositoryResponse<ChatRoomEntity?> response = new RepositoryResponse<ChatRoomEntity>();
            try{
                response.Data = _dbContext.ChatRoom.Where(r => r.Code.Equals(code)).FirstOrDefault();
                response.IsSuccess = true;
            }catch(Exception ex){
                response.IsSuccess = false;
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return response;
        }
        
    }
}