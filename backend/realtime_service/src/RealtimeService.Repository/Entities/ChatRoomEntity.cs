using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RealtimeService.Repository.Entities
{
    [Table("chat_rooms", Schema = "public")]
    public class ChatRoomEntity
    {
        [Column(name:"id"), Key]
        public string ID {get;set;}

        [Column(name:"name")]
        public string Name {get;set;}

        [Column(name:"creator_user_id")]
        public string CreatorUserID {get;set;}

        [Column(name:"created_at")]
        public DateTime CreatedAt {get;set;}
        
        [Column(name:"code")]
        public string Code {get;set;}

        
        [Column(name:"updated_at")]
        public DateTime? UpdatedAt {get;set;}

        [Column(name:"deleted_at")]
        public DateTime? DeletedAt {get;set;}
    }
}